(function($){
	$('.front_page div.post_box').removeAttr('class');
	$('.front_page div.format_text').removeAttr('class');
	$('.front_page div.teaser:nth-child(even)').addClass('teaser_right');
	$('.bloggers #archive_intro').next('.teasers_box').addClass('top');
})(jQuery);