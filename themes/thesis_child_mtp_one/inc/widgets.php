<?php

class mtp_widget_image extends WP_Widget
{
	public function __construct()
	{
		$widget_ops = array(
			'classname'   => 'widget_image',
			'description' => __('Insert an image into your sidebar.'),
		);

		parent::__construct('image',__('Image'),$widget_ops);

		add_action('admin_enqueue_scripts',array($this,'scripts'));
	}

	public function form($instance)
	{
		$instance = wp_parse_args((array)$instance,array(
			'alt'    => '',
			'height' => '',
			'link'   => '',
			'target' => '',
			'title'  => '',
			'src'    => '',
			'width'  => '',
		));

		$alt    = strip_tags($instance['alt']);
		$height = isset($instance['height']) ? absint($instance['height']) : 1;
		$link   = strip_tags($instance['link']);
		$target = isset($instance['target']) ? $instance['target'] : '';
		$title  = strip_tags($instance['title']);
		$src    = strip_tags($instance['src']);
		$width  = isset($instance['width']) ? absint($instance['width']) : 1; ?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('src'); ?>"><?php _e('Image URL:'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('src'); ?>" name="<?php echo $this->get_field_name('src'); ?>" type="text" value="<?php echo $src; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('link'); ?>"><?php _e('Link:'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('link'); ?>" name="<?php echo $this->get_field_name('link'); ?>" type="text" value="<?php echo $link; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('target'); ?>"><?php _e('Open in New Window:') ?></label>
			<input id="<?php echo $this->get_field_id('target'); ?>" name="<?php echo $this->get_field_name('target'); ?>" type="checkbox" <?php checked(isset($instance['target']) ? $instance['target'] : 0); ?> />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('alt'); ?>"><?php _e('Alternative Text:'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('alt'); ?>" name="<?php echo $this->get_field_name('alt'); ?>" type="text" value="<?php echo $alt; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('height'); ?>"><?php _e('Height:'); ?></label>
			<input id="<?php echo $this->get_field_id('height'); ?>" name="<?php echo $this->get_field_name('height'); ?>" max="999" min="1" site="3" type="number" value="<?php echo $height; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('width'); ?>"><?php _e('Width:'); ?></label>
			<input id="<?php echo $this->get_field_id('width'); ?>" name="<?php echo $this->get_field_name('width'); ?>" max="999" min="1" site="3" type="number" value="<?php echo $width; ?>" />
		</p>
	<?php }

	public function update($new_instance,$old_instance)
	{
		$instance           = $old_instance;
		$instance['alt']    = strip_tags($new_instance['alt']);
		$instance['height'] = (int)$new_instance['height'];
		$instance['link']   = strip_tags($new_instance['link']);
		$instance['target'] = isset($new_instance['target']);
		$instance['title']  = strip_tags($new_instance['title']);
		$instance['src']    = strip_tags($new_instance['src']);
		$instance['width']  = (int)$new_instance['width'];

		return $instance;
	}

	public function widget($args,$instance)
	{
		extract($args);

		$alt    = empty($instance['alt']) ? 'Image' : $instance['alt'];
		$height = empty($instance['height']) ? '200' : $instance['height'];
		$link   = apply_filters('widget_text',empty($instance['link']) ? '' : $instance['link'], $instance);
		$target = empty($instance['target']) ? '' : 'target="_blank"';
		$title  = apply_filters('widget_title',empty($instance['title']) ? '' : $instance['title'],$instance,$this->id_base);
		$src    = apply_filters('widget_text',empty($instance['src']) ? '' : $instance['src'], $instance);
		$width  = empty($instance['width']) ? '300' : $instance['width'];

		echo $before_widget;

		if ($title)
		{
			echo $before_title . $title . $after_title;
		}

		if ($link)
		{
			echo '<div class="image"><a href="'.$link.'" '.$target.'><img alt="'.$alt.'" class="aligncenter" height="'.$height.'" src="'.$src.'" width="'.$width.'" /></a></div>';
		}
		else
		{
			echo '<div class="image"><img alt="'.$alt.'" class="aligncenter" height="'.$height.'" src="'.$src.'" width="'.$width.'" /></div>';
		}

	 	echo $after_widget;
	}

	public function scripts()
	{
		if ('widgets' == get_current_screen()->id)
		{
			wp_register_script('widget_image_js',get_theme_root_uri().'/thesis_child_theme_two/inc/widget-image.js',array('jquery','media-upload','thickbox'));

			wp_enqueue_script('jquery');
			wp_enqueue_script('thickbox');
			wp_enqueue_script('media-upload');
			wp_enqueue_script('widget_image_js');

			wp_enqueue_style('thickbox');
		}
	}
}

register_widget('mtp_widget_image');