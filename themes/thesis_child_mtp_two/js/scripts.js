/**
 * [Bloggers Page]
 * @param  {[type]} $ [jQuery]
 * @return {[type]}   [description]
 */
;(function($){
	$('.bloggers #archive_intro').next('.teasers_box').addClass('top');
})(jQuery);

/**
 * [Read more/less]
 * @param  {[type]} $ [jQuery]
 * @return {[type]}   [description]
 */
;(function($){
	var $hidden = $('#archive_intro .hidden'),
		$more   = $('#archive_intro .more-btn'),
		$less   = $('#archive_intro .less-btn');

	$hidden.hide();
	$less.hide();
	$more.find('a').click(function(e){
		e.preventDefault();
		$more.hide();
		$hidden.slideToggle('200',function(){
			$less.fadeToggle();
		});
	});
	$less.find('a').click(function(e){
		e.preventDefault();
		$less.hide();
		$hidden.slideToggle('200',function(){
			$more.fadeToggle();
		});
	});
})(jQuery);