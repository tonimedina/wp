<?php
// Iinitial sanity check
if (!defined('ABSPATH'))
{
	die('Please do not directly access this file');
}

// Bring in the main functions file so we have access to all the yummy Thesis goodness
include_once(TEMPLATEPATH.'/functions.php');

// We encourage you to set up a class. classes are a VERY important part of 2.0, so you need to be familiar with them.
class mtp_mdhombre_child_theme extends thesis_custom_loop
{

	public function __construct()
	{
		// Run the parent constructor so we can access the thesis custom loop api
		parent::__construct();

		// Run the main init
		add_action('init',array($this,'init'));
	}

	public function init()
	{
		// Actions and filters that will run on init.
		$this->actions();
		$this->filters();
	}

	public function actions()
	{
		// This will force thesis to generate CSS when the user switches to the child
		add_action('after_switch_theme','thesis_generate_css');

		// This will add extra body classes to all templates
		add_action('thesis_body_classes','mtp_mdhombre_thesis_body_classes');
		
		// This will add the search form bellow the header
		add_action('thesis_hook_after_header','thesis_search_form');

		// This will remove the nav menu from its default location and add it after the header
		remove_action('thesis_hook_before_header','thesis_nav_menu');
		add_action('thesis_hook_after_header','thesis_nav_menu');

		// This will generate the breadcrumbs
		add_action('thesis_hook_before_content','mtp_mdhombre_breadcrumbs');			

		// This will add the featured image
		add_action('thesis_hook_before_teaser_headline','mtp_mdhombre_featured_image');

		// This will add the user avatar to the post byline
		add_action('thesis_hook_after_teaser_headline','mtp_mdhombre_byline_avatar');
		add_action('thesis_hook_byline_item','mtp_mdhombre_byline_avatar');

		// This will add the addthis button
		add_action('thesis_hook_after_teaser','mtp_mdhombre_after_teaser');

		// This will display related posts by category after the post content
		add_action('thesis_hook_after_post','mtp_mdhombre_related_posts');

		// This will display the post tags
		add_action('thesis_hook_after_post','mtp_mdhombre_tag_list');

		// This will display the facebook comments - http://developers.facebook.com/docs/reference/plugins/comments/]
		add_action('thesis_hook_after_post','mtp_mdhombre_fb_comments');

		// This will remove the thesis attribution in the footer area
		remove_action('thesis_hook_footer','thesis_attribution');

		// This will display the widget inside the footer sidebars in the footer of the site
		add_action('thesis_hook_footer','mtp_mdhombre_footer_sidebars','1');

		// This will enqueue all the required scripts
		add_action('wp_enqueue_scripts','mtp_mdhombre_wp_enqueue_scripts');

		// This will apply our custom templates
		remove_action('thesis_hook_custom_template', 'thesis_custom_template_sample');
		add_action('thesis_hook_custom_template', 'mtp_mdhombre_custom_template');

		//404 customized page
		remove_action('thesis_hook_404_title','thesis_404_title');
		add_action('thesis_hook_404_title','custom_thesis_404_title');
		remove_action('thesis_hook_404_content','thesis_404_content');
		add_action('thesis_hook_404_content','custom_thesis_404_content');
	}

	public function filters()
	{	
		// This will update the doctype
		add_filter('thesis_doctype','mtp_mdhombre_thesis_doctype');

		// This will remove the profile attribute from head tag
		add_filter('thesis_head_profile','mtp_mdhombre_thesis_head_profile');

		// This will set a default excerpt length
		add_filter('excerpt_length','mtp_mdhombre_excerpt_length');
		
		// This will extend the user options page
		add_filter('user_contactmethods','mtp_mdhombre_user_contact_methods',10,1);
		
		//
		add_filter('thesis_archive_intro','mtp_mdhombre_archive_intro');

		// This will modify the archive intro section, only for the author page template in order to show the user meta info
		add_filter('thesis_archive_intro','mtp_mdhombre_author_archive_intro');

		// This will add a rel=nofollow to comment reply links
		add_filter('comment_reply_link','mtp_mdhombre_comment_reply_link');
		
		// This will hide the headline area if it is the front page
		add_filter('thesis_show_headline_area','mtp_mdhombre_thesis_show_headline_area');
	}
}

new mtp_mdhombre_child_theme;

/**
 * [Action after_setup_theme]
 * @return [type] [Several WP Functions]
 */
function mtp_mdhombre_after_setup_theme()
{
	// This will load the required files to translate the theme
	load_child_theme_textdomain('thesis',get_stylesheet_directory().'/lang');

	// This will add support for post formats
	add_theme_support('post-formats',array('gallery'));

	// This will add support for post thumbnails
	add_theme_support('post-thumbnails');

	// This will add new image sizes
	set_post_thumbnail_size(300,226,true);
	add_image_size('default-thumbnail',300,226,true);
	add_image_size('big-thumbnail',359,239,true);

	// This will get the widgets
	require(ABSPATH.'wp-content/themes/thesis_child_mtp_two/inc/widgets.php');

	// This will get the shortcodes
	require(ABSPATH.'wp-content/themes/thesis_child_mtp_two/inc/shortcodes.php');
}

add_action('after_setup_theme','mtp_mdhombre_after_setup_theme');

/**
 * [Action widgets_init]
 * @return [type] [The Sidebars]
 */
function mtp_mdhombre_widgets_init()
{
	unregister_sidebar('sidebar-2');

	register_sidebar(array(
		'name'          => 'Footer Area Top',
		'id'            => 'footer_area_top',
		'description'   => __('Footer Widgets Top','thesis'),
		'before_widget' => '<li class="widget %2$s" id="%1$s">',
		'after_widget'  => '</li>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>'
	));

	register_sidebar(array(
		'name'          => 'Footer Area 1',
		'id'            => 'footer_area_one',
		'description'   => __('Footer Widgets Left','thesis'),
		'before_widget' => '<li class="widget %2$s" id="%1$s">',
		'after_widget'  => '</li>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>'
	));

	register_sidebar(array(
		'name'          => 'Footer Area 2',
		'id'            => 'footer_area_two',
		'description'   => __('Footer Widgets Left Middle','thesis'),
		'before_widget' => '<li class="widget %2$s" id="%1$s">',
		'after_widget'  => '</li>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>'
	));

	register_sidebar(array(
		'name'          => 'Footer Area 3',
		'id'            => 'footer_area_three',
		'description'   => __('Footer Widgets Middle','thesis'),
		'before_widget' => '<li class="widget %2$s" id="%1$s">',
		'after_widget'  => '</li>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>'
	));

	register_sidebar(array(
		'name'          => 'Footer Area 4',
		'id'            => 'footer_area_four',
		'description'   => __('Footer Widgets Right Middle','thesis'),
		'before_widget' => '<li class="widget %2$s" id="%1$s">',
		'after_widget'  => '</li>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>'
	));

	register_sidebar(array(
		'name'          => 'Footer Area 5',
		'id'            => 'footer_area_five',
		'description'   => __('Footer Widgets Right','thesis'),
		'before_widget' => '<li class="widget %2$s" id="%1$s">',
		'after_widget'  => '</li>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>'
	));

	register_sidebar(array(
		'name'          => 'Footer Area Bottom',
		'id'            => 'footer_area_bottom',
		'description'   => __('Footer Widgets Bottom','thesis'),
		'before_widget' => '<li class="widget %2$s" id="%1$s">',
		'after_widget'  => '</li>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>'
	));
}

add_action('widgets_init','mtp_mdhombre_widgets_init');

/**
 * [Action thesis_body_classes]
 * @param  [array] $classes [Thesis Default (custom)]
 * @return [array]          [Extra body classes]
 */
function mtp_mdhombre_thesis_body_classes($classes)
{
	if (is_404())            { $classes[] = 'error404'; }
	if (is_archive())        { $classes[] = 'archive'; }
	if (is_attachment())     { $classes[] = 'attachment'; }
	if (is_author())         { $classes[] = 'author'; }
	if (is_category())       { $classes[] = 'category'; }
	if (is_comments_popup()) { $classes[] = 'comments_popup'; }
	if (is_date())           { $classes[] = 'date'; }
	if (is_day())            { $classes[] = 'day'; }
	if (is_feed())           { $classes[] = 'feed'; }
	if (is_front_page())     { $classes[] = 'front_page'; }
	if (is_home())           { $classes[] = 'home'; }
	if (is_month())          { $classes[] = 'month'; }
	if (is_page())           { $classes[] = 'page'; }
	if (is_page_template())  { $classes[] = 'page_template'; }
	if (is_preview())        { $classes[] = 'preview'; }
	if (is_rtl())            { $classes[] = 'rtl'; }
	if (is_search())         { $classes[] = 'search'; }
	if (is_single())         { $classes[] = 'single'; }
	if (is_sticky())         { $classes[] = 'sticky'; }
	if (is_tag())            { $classes[] = 'tag'; }
	if (is_tax())            { $classes[] = 'taxonomy'; }
	if (is_time())           { $classes[] = 'time'; }
	if (is_trackback())      { $classes[] = 'trackback'; }
	if (is_year())           { $classes[] = 'year'; }

	return $classes;
}

/**
 * [Action thesis_hook_after_header]
 * @return [string] [The Breadcumbs]
 */
function mtp_mdhombre_breadcrumbs()
{
	global $post;

	$home_url  = esc_url(home_url('/'));
	$year  = get_the_time('Y');
	$month = get_the_time('m');
	$day   = get_the_time('d');

	if (!is_front_page())
	{
		echo '<div class="breadcrumbs"><p><a href="'.$home_url.'" rel="home">'.__('Home','thesis').'</a> / ';

		if (is_404())
		{
			_e('Error 404','thesis');
		}
		else if (is_archive())
		{
			if (is_author())
			{
				$curauth = get_query_var('author_name') ? get_user_by('slug',get_query_var('author_name')) : get_userdata(get_query_var('author'));

				_e('Author','thesis');
				echo ' / '.$curauth->display_name;
			}
			else if (is_date())
			{
				if (is_day())
				{
					echo '<a href="'.$home_url.$year.'/"">'.$year.'</a> / <a href="'.$home_url.$year.'/'.$month.'/">'.$month.'</a> / '.$day.'';
				}
				else if (is_month())
				{
					echo '<a href="'.$home_url.$year.'/"">'.$year.'</a> / '.$month.'';
				}
				else if (is_year())
				{
					the_time('Y');
				}
			}
			else if (is_category())
			{
				$curcat = get_category(get_query_var('cat'),false);

				if ($curcat->parent != 0)
				{
					echo get_category_parents($curcat->parent,true,' / ');
				}

				echo single_cat_title('',false);
			}
			else if (is_tag())
			{
				_e('Tag','thesis');
				echo ' / '.single_tag_title('',false);
			}
		}
		else if (is_page())
		{
			if ($post->post_parent)
			{
				$parent = $post->post_parent;
				$breadcrumbs = array();

				while ($parent)
				{
					$page = get_page($parent);
					$breadcrumbs[] = '<a href="'.get_permalink($page->ID).'">'.get_the_title($page->ID).'</a>';
					$parent = $page->post_parent;
				}

				$breadcrumbs = array_reverse($breadcrumbs);

				for ($i=0;$i<count($breadcrumbs);$i++)
				{ 
					echo $breadcrumbs[$i];

					if ($i != count($breadcrumbs)-1)
					{
						echo ' / ';
					}
				}

				echo ' / '. get_the_title();
			}
			else
			{
				echo get_the_title();
			}
		}
		else if (is_search())
		{
			_e('Search','thesis');
			echo ' / '.get_search_query();
		}
		else if (is_single())
		{
			the_category(' / ');
			echo ' / '.get_the_title();
		}

		echo '</p></div>';
	}
}

/**
 * [mtp_mdhombre_featured_image description]
 * @return [string] [WP Featured Image]
 */
function mtp_mdhombre_featured_image()
{
	if (has_post_thumbnail())
	{
		global $post;

		$attr = array('class'=>'thumb aligncenter');

		echo '<a class="post_image_link" href="'.get_permalink().'" title="'.esc_attr(sprintf(__('Permalink to %s','thesis'),the_title_attribute('echo=0'))).'">'.get_the_post_thumbnail($post->ID,'default-thumbnail',$attr).'</a>';
	}
}

/**
 * [Action thesis_hook_after_teaser_headline]
 * @return [string] [The User Avatar]
 */
function mtp_mdhombre_byline_avatar()
{
	echo '<span class="teaser_avatar">'.get_avatar(get_the_author_id(),26).'</span>';
}

/**
 * [Action thesis_hook_after_teaser]
 * @return [string] [AddThis Plugin + Comments]
 */
function mtp_mdhombre_after_teaser()
{
	global $post;

	echo '<p>'; 

	thesis_teaser_link($post_count, $post_image);

	echo '</p><ul class="st_comments"><li><span class="st_fblike_hcount" displayText="Facebook Like" st_url="'.get_permalink().'" st_title="'.get_the_title().'"></span></li><li><span class="st_twitter_hcount" displayText="Tweet" st_url="'.get_permalink().'" st_title="'.get_the_title().'"></span></li><li><span class="st_plusone_hcount" displayText="Google +1" st_url="'.get_permalink().'" st_title="'.get_the_title().'"></span></li><li><a class="teaser_comments" href="'.get_permalink().'#comments" rel="nofollow">'.get_comments_number($post->ID).'</a></li></ul>';
}

/**
 * [Action thesis_hook_after_post]
 * @return [string] [Related Posts]
 */
function mtp_mdhombre_related_posts()
{
	if (is_single())
	{
		global $post;

		$curpost    = $post->ID;
		$categories = get_the_category();
		$tags       = get_the_tags();
		$thumb      = get_post_meta($post->ID,'thesis_thumb',true);
		$alt        = get_post_meta($post->ID,'thesis_thumb_alt',true);

		foreach ($categories as $category)
		{
			if ($tags)
			{
				$posts = get_posts('numberposts=4&category='.$category->term_id.'&tags='.$tags.'&exclude='.$curpost.'');
			}
			else
			{
				$posts = get_posts('numberposts=4&category='.$category->term_id.'&exclude='.$curpost.'');
			}
		}

		if (!empty($posts))
		{
			echo '<div class="related-posts"><h3 class="section-title">'.__('Related Posts','thesis').'</h3><ul>';

			foreach ($posts as $post)
			{
				if (has_post_thumbnail())
				{
					echo '<li><a href="'.get_permalink().'" rel="bookmark" title="'.esc_attr(sprintf(__('Permalink to %s','thesis'),the_title_attribute('echo=0'))).'"><span class="img">'.get_the_post_thumbnail($post->ID,array(140,140)).'</span><span class="title">'.substr(the_title($before = '',$after = ' ...',false),0,30).'</span></a></li>';
				}
				else
				{
					echo '<li><a href="'.get_permalink().'" rel="bookmark" title="'.esc_attr(sprintf(__('Permalink to %s','thesis'),the_title_attribute('echo=0'))).'"><span class="img"><img alt="'.$alt.'" height="140" src="'.$thumb.'" width="140"></span><span class="title">'.substr(the_title($before = '',$after = ' ...',false),0,30).'</span></a></li>';
				}
			}

			echo '</ul></div>';
		}

		wp_reset_query();
	}
}

/**
 * [Action thesis_hook_after_post]
 * @return [string] [The Post Tags]
 */
function mtp_mdhombre_tag_list()
{
	$tag_list = get_the_tag_list('<p>',', ','</p>');

	if (is_single() && !empty($tag_list))
	{
		echo '<div class="post-tags"><h3 class="section-title">'.__('Tags','thesis').'</h3>'.$tag_list.'</div>';
	}
}

/**
 * [Action thesis_hook_after_post]
 * @return [string] [The Facebook Comment Box]
 */
function mtp_mdhombre_fb_comments()
{
	if (is_single())
	{
		echo '<h3 class="section-title fb">'.__('Comments','thesis').'</h3><div id="fb-root"></div><script></script><script type="text/javascript">(function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(d.getElementById(id))return;js=d.createElement(s);js.id=id;js.src="//connect.facebook.net/en_US/all.js#xfbml=1";fjs.parentNode.insertBefore(js,fjs);}(document,"script","facebook-jssdk"));</script><div class="fb-comments" data-href="'.get_permalink().'" data-num-posts="10" data-width="640"></div>';
	}
}

/**
 * [Count the number of footer sidebars to enable dynamic classes for the footer]
 * @return [string] [The Class Number]
 */
function mtp_mdhombre_footer_classes()
{
	$count = 0;

	if (is_active_sidebar('footer_area_one'))
	{
		$count++;
	}

	if (is_active_sidebar('footer_area_two'))
	{
		$count++;
	}

	if (is_active_sidebar('footer_area_three'))
	{
		$count++;
	}

	if (is_active_sidebar('footer_area_four'))
	{
		$count++;
	}

	if (is_active_sidebar('footer_area_five'))
	{
		$count++;
	}

	$class = '';

	switch ($count)
	{
		case '1':
			$class = 'one';
			break;

		case '2':
			$class = 'two';
			break;

		case '3':
			$class = 'three';
			break;

		case '4':
			$class = 'four';
			break;

		case '5':
			$class = 'five';
			break;
	}

	if ($class)
	{
		echo $class;
	}
}

/**
 * [Action thesis_hook_footer]
 * @return [string] [The Footer Sidebars]
 */
function mtp_mdhombre_footer_sidebars()
{
	if (is_front_page() && is_active_sidebar('footer_area_top'))
	{
		echo '<div class="footer-widget-block"><div class="my-footer-top footer-widgets sidebar"><ul class="sidebar_list">';
		thesis_default_widget('footer_area_top');
		echo '</ul></div></div>';
	}

	if (is_active_sidebar('footer_area_one') || is_active_sidebar('footer_area_two') || is_active_sidebar('footer_area_three') || is_active_sidebar('footer_area_four') || is_active_sidebar('footer_area_five'))
	{
		echo '<div class="footer-widget-block ';
		mtp_mdhombre_footer_classes();
		echo '">';

		if (is_active_sidebar('footer_area_one'))
		{
			echo '<div class="my-footer-one footer-widgets sidebar"><ul class="sidebar_list">';
			thesis_default_widget('footer_area_one');
			echo '</ul></div>';
		}
		
		if (is_active_sidebar('footer_area_two'))
		{
			echo '<div class="my-footer-two footer-widgets sidebar"><ul class="sidebar_list">';
			thesis_default_widget('footer_area_two');
			echo '</ul></div>';
		}
		
		if (is_active_sidebar('footer_area_three'))
		{
			echo '<div class="my-footer-three footer-widgets sidebar"><ul class="sidebar_list">';
			thesis_default_widget('footer_area_three');
			echo '</ul></div>';
		}

		if (is_active_sidebar('footer_area_four'))
		{
			echo '<div class="my-footer-four footer-widgets sidebar"><ul class="sidebar_list">';
			thesis_default_widget('footer_area_four');
			echo '</ul></div>';
		}

		if (is_active_sidebar('footer_area_five'))
		{
			echo '<div class="my-footer-five footer-widgets sidebar"><ul class="sidebar_list">';
			thesis_default_widget('footer_area_five');
			echo '</ul></div>';
		}

		echo '</div>';
	}

	if (is_active_sidebar('footer_area_bottom'))
	{
		echo '<div class="footer-widget-block"><div class="my-footer-bottom footer-widgets sidebar"><ul class="sidebar_list">';
		thesis_default_widget('footer_area_bottom');
		echo '</ul></div></div>';
	}
}

/**
 * [mtp_mdhombre_wp_enqueue_scripts description]
 * @return [type] [description]
 */
function mtp_mdhombre_wp_enqueue_scripts()
{
	wp_deregister_script('jquery');

	wp_register_script('jquery','http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js');
	wp_enqueue_script('jquery');

	wp_register_script('mtp_mdhombre_scripts',ABSPATH.'wp-content/themes/thesis_child_mtp_two/js/scripts.js',$deps,$ver,true);
	wp_enqueue_script('mtp_mdhombre_scripts');

	if (is_home() || is_front_page() || is_archive())
	{
		wp_register_script('share_this_buttons_js','http://w.sharethis.com/button/buttons.js',$deps,$ver,true);
		wp_enqueue_script('share_this_buttons_js');
	}
}

/**
 * [Action thesis_hook_custom_template]
 * @return [string] [the template]
 */
function mtp_mdhombre_custom_template()
{
	if (is_page('bloggers'))
	{
		$users = get_users(array(
			'number'  => 10,
			'order'   => DESC,
			'orderby' => 'post_count'
		));
		$count = 0; ?>

		<div id="content">
			<?php mtp_mdhombre_breadcrumbs(); ?>
			<div id="archive_intro">
				<h1><?php _e('Bloggers','thesis'); ?></h1>
			</div>
			<div class="teasers_box">
				<?php
				foreach ($users as $user)
				{
					$count++;

					$id     = $user->ID;
					$url    = get_bloginfo('url');
					$login  = $user->user_login;
					$avatar = get_avatar($id,300);
					$name   = $user->display_name;
					$meta   = get_user_meta($id);
					$desc   = $meta['description']['0'];

					if ($count % 2 == 0)
					{
						$before = '<div class="teaser teaser_right" id="'.$id.'">';
						$after  = '</div></div><div class="teasers_box">';
					}
					else
					{
						$before = '<div class="teaser" id="'.$id.'">';
						$after  = '</div>';
					}
				
					echo $before; ?>
					<a class="post_image_link" href="<?php echo $url; ?>/author/<?php echo $login; ?>"><?php echo $avatar; ?></a>
					<h2 class="entry-title"><a href="<?php echo $url; ?>/author/<?php echo $login; ?>" rel="bookmark"><?php echo $name; ?></a></h2>
					<div class="format_teaser entry-content">
						<p><?php substr($desc,0,150); ?> [...]</p>
					</div>
					<?php echo $after;
				} ?>

			</div>
		</div>
		<div id="sidebars">
			<?php thesis_build_sidebars(); ?>
		</div>
	<?php }
	else if (is_front_page()) { ?>
		<div id="content">
			<?php if (function_exists('get_thethe_image_slider'))
			{
				print get_thethe_image_slider('Front Page Slider');
			}

			while (have_posts())
			{
				the_post();
				the_content();
			}

			mtp_mdhombre_breadcrumbs(); ?>
		</div>

		<div id="sidebars">
			<?php thesis_build_sidebars(); ?>
		</div>
	<?php }
}

/* Custom 404 Page 
-------------------------------------------------------------------------------------------------*/
function custom_thesis_404_title() {
        _e('Este contenido no est&aacute; disponible.','theis');
}

function custom_thesis_404_content() {

        $homepage = get_bloginfo('url');
        $blogname = get_bloginfo('name');
        $before = $_SERVER["HTTP_REFERER"];
        echo '<p>'._x('Ha habido un error y la página a la que intentas acceder no est&aacute; 
                temporalmente disponible. Puedes tratar de localizar el contenido mediante el buscador, 
                o bien navegar entre las categor&iacute;as disponibles en el sitio. Si lo deseas, puedes 
                volver a la portada de','').' <a href='.$homepage.'>'.$blogname.'</a> '._x('o al','').
                ' <a href='.$before.'>'._x('contenido anterior.','').'</a></p>';

        wp_list_categories(array(
                'show_option_all'    => '',
                'orderby'            => 'name',
                'order'              => 'ASC',
                'style'              => 'list',
                'show_count'         => 0,
                'hide_empty'         => 0,
                'use_desc_for_title' => 1,
                'child_of'           => 0,
                'feed'               => '',
                'feed_type'          => '',
                'feed_image'         => '',
                'exclude'            => '',
                'exclude_tree'       => '',
                'include'            => '',
                'hierarchical'       => true,
                'title_li'           => __( 'Categorias' ),
                'show_option_none'   => __('No hay categorias'),
                'number'             => null,
                'echo'               => 1,
                'depth'              => 0,
                'current_category'   => 0,
                'pad_counts'         => 0,
                'taxonomy'           => 'category',
                'walker'             => null
        ));

}

/**
 * [Filter thesis_doctype]
 * @return [string] [HTML5 doctype]
 */
function mtp_mdhombre_thesis_doctype()
{
	return '<!doctype html>';
}

/**
 * [Filter thesis_head_profile]
 * @return [string] [Empty]
 */
function mtp_mdhombre_thesis_head_profile()
{
	return '';
}

/**
 * [Filter excerpt_length]
 * @param  [int] $length [WP Default]
 * @return [int]         [The Length]
 */
function mtp_mdhombre_excerpt_length($length)
{
	return 15;
}

/**
 * [Filter user_contactmethods]
 * @param  [array] $user_contactmethods [WP Default]
 * @return [array]                      [New options fields]
 */
function mtp_mdhombre_user_contact_methods($user_contactmethods)
{
	$user_contactmethods['facebook'] = 'Facebook';
	$user_contactmethods['google_plus'] = 'Google+';
	$user_contactmethods['pinterest'] = 'Pinterest';
	$user_contactmethods['twitter'] = 'Twitter';

	return $user_contactmethods;
}

function mtp_mdhombre_archive_intro($output)
{
	if (strpos($output,'<!-- more -->') === false)
	{
		return $content;
	}
	
	$output = str_replace(array('<br>','<br />','<p>','</p>'),'',$output);
	$output = explode('<!-- more -->',$output);

	if (count($output) == 2)
	{
		$toggle_visible = '<p class="more-btn"><a href="#">'._x('Leer Más...','thesis').'</a></p>';
		$toggle_hidden  = '<p class="less-btn"><a href="#">'._x('Leer Menos...','thesis').'</a></p>';
		$visible        = $output[0].' '.$toggle_visible;
		$hidden         = '<div class="hidden">'.$output[1].' '.$toggle_hidden.'</div>';
		$output         = $visible.$hidden;
	}
	else
	{
		$content = implode(' ',$output);
	}

	return $output;
}

/**
 * [Filter thesis_archive_intro]
 * @return [string] [new archive info]
 */
function mtp_mdhombre_author_archive_intro($output)
{
	global $wpdb;

	if (is_author())
	{
		$curauth = get_query_var('author_name') ? get_user_by('slug',get_query_var('author_name')) : get_userdata(get_query_var('author'));
		$stories = count_user_posts($curauth->ID);

		$output = '<div id="archive_intro"><div class="user_avatar">';
		$output .= get_avatar(get_the_author_meta('ID',$curauth->ID),150);
		$output .= '</div><div class="user_contact_info"><h1>'.$curauth->display_name.'</h1>';

		if ($curauth->user_registered && $stories)
		{
			$output .= '<p class="user_meta">'.__('Contributing since:','thesis').' '.$curauth->user_registered.'<br>'.__('Stories written:','thesis').' '.$stories.'</p>';
		}
		else if ($curauth->user_registered)
		{
			$output .= '<p class="user_meta">'.__('Contributing since:','thesis').' '.$curauth->user_registered.'</p>';
		}
		else if ($stories)
		{
			$output .= '<p class="user_meta">'.__('Stories written:','thesis').' '.$stories.'</p>';
		}

		if ($curauth->user_url || $curauth->user_email || $curauth->facebook || $curauth->twitter || $curauth->google_plus || $curauth->pinterest)
		{
			$output .= '<ul>';

			if ($curauth->user_url)
			{
				$output .= '<li><a class="user_url" href="'.$curauth->user_url.'" rel="contact nofollow" target="_blank">'.$curauth->user_url.'</a></li>';
			}

			if ($curauth->user_email)
			{
				$output .= '<li><a class="user_email" href="mailto:'.$curauth->user_email.'" rel="contact nofollow" target="_blank">'.$curauth->user_email.'</a></li>';
			}

			if ($curauth->facebook)
			{
				$output .= '<li><a class="user_facebook" href="http://facebook.com/'.$curauth->facebook.'" rel="contact nofollow" target="_blank">'.$curauth->facebook.'</a></li>';
			}

			if ($curauth->twitter)
			{
				$output .= '<li><a class="user_twitter" href="http://twitter.com/'.$curauth->twitter.'" rel="contact nofollow" target="_blank">'.$curauth->twitter.'</a></li>';
			}

			if ($curauth->google_plus)
			{
				$output .= '<li><a class="user_google_plus" href="http://plus.google.com/'.$curauth->google_plus.'" rel="contact nofollow" target="_blank">'.$curauth->google_plus.'</a></li>';
			}

			if ($curauth->pinterest)
			{
				$output .= '<li><a class="user_pinterest" href="http://pinterest.com/'.$curauth->pinterest.'" rel="contact nofollow" target="_blank">'.$curauth->pinterest.'</a></li>';
			}
			
			$output .= '</ul>';
		}

		$output .= '</div>';

		if ($curauth->description)
		{
			$output .= '<div class="user_description"><h4>'.__('Biography','thesis').'</h4><p>'.$curauth->description.'</p></div>';
		}

		$output .= '</div>';
	}

	return $output;
}

/**
 * [Filter comment_reply_link]
 * @param  [string] $link [Thesis Default]
 * @return [string]       [Anchor tags with rel=nofollow]
 */
function mtp_mdhombre_comment_reply_link($link)
{
	global $user_ID;

	if (get_option('comment_registration') && !$user_ID)
	{
		return $link;
	}
	else
	{
		return str_replace('")\'>','")\' rel=\'nofollow\'>',$link);
	}
}

/**
 * [Filter thesis_show_headline_area]
 * @return [boolean] [true - frontpage = false]
 */
function mtp_mdhombre_thesis_show_headline_area()
{
	return (is_front_page()) ? false : true;
}

/**
 * [RssImporter_custom_feed]
 */
function RssImporter_custom_feed()
{
	global $post;

    header('Content-Type: '.feed_content_type('rss-http').'; charset='.get_option('blog_charset'),true);
    $more = 1;
    echo '<?xml version="1.0" encoding="'.get_option('blog_charset').'"?'.'>'; ?>

    <rss version="2.0"
        xmlns:content="http://purl.org/rss/1.0/modules/content/"
        xmlns:wfw="http://wellformedweb.org/CommentAPI/"
        xmlns:dc="http://purl.org/dc/elements/1.1/"
        xmlns:atom="http://www.w3.org/2005/Atom"
        xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
        xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
        <?php do_action('rss2_ns'); ?>
    >

    <channel>
        <title><?php bloginfo_rss('name'); wp_title_rss(); ?></title>
        <atom:link href="<?php self_link(); ?>" rel="self" type="application/rss+xml" />
        <link><?php bloginfo_rss('url') ?></link>
        <description><?php bloginfo_rss('description'); ?></description>
        <lastBuildDate><?php echo mysql2date('D, d M Y H:i:s +0000', get_lastpostmodified('GMT'),false); ?></lastBuildDate>
        <language><?php echo get_option('rss_language'); ?></language>
        <sy:updatePeriod><?php echo apply_filters('rss_update_period','hourly'); ?></sy:updatePeriod>
        <sy:updateFrequency><?php echo apply_filters('rss_update_frequency','1'); ?></sy:updateFrequency>
        <?php
        do_action('rss2_head');
        while(have_posts()) : the_post();
        ?>
        <item>
        	<?php if (has_post_thumbnail()) : ?>
        	<thumbnail><?php echo the_post_thumbnail($post->ID,'thumbnail'); ?></thumbnail>
        	<?php endif; ?>
            <title><?php the_title_rss(); ?></title>
            <link><?php the_permalink_rss(); ?></link>
            <comments><?php comments_link_feed(); ?></comments>
            <pubDate><?php echo mysql2date('D, d M Y H:i:s +0000',get_post_time('Y-m-d H:i:s',true),false); ?></pubDate>
            <?php the_category_rss('rss2'); ?>
            <guid isPermaLink="false"><?php the_guid(); ?></guid>
            <?php if (get_option('rss_use_excerpt')) : ?>
            <description><![CDATA[<?php the_excerpt_rss(); ?>]]></description>
            <?php else : ?>
            <description><![CDATA[<?php the_excerpt_rss(); ?>]]></description>
            <?php if (strlen($post->post_content) > 0) : ?>
            <content:encoded><![CDATA[<?php the_content_feed('rss2'); ?>]]></content:encoded>
            <?php else : ?>
            <content:encoded><![CDATA[<?php the_excerpt_rss(); ?>]]></content:encoded>
            <?php endif; endif; ?>
            <wfw:commentRss><?php echo esc_url(get_post_comments_feed_link(null,'rss2')); ?></wfw:commentRss>
            <slash:comments><?php echo get_comments_number(); ?></slash:comments>
            <?php rss_enclosure(); do_action('rss2_item'); ?>
        </item>
        <?php endwhile; ?>
    </channel>
    </rss>
<?php }

remove_all_actions('do_feed_rss2');
add_action('do_feed_rss2','RssImporter_custom_feed',10,1);

?>