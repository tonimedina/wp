jQuery(document).ready(function($){
	$('input[id$="-src"]').each(function(){
		$(this).click(function(){
			var widget_id = $(this).attr('name').match(/[\d]/);

			tb_show('Add Image','media-upload.php?refererwidget-image&amp;type=image&amp;TB_iframe=true&amp;post_id=0',false);

			window.send_to_editor = function(html){
				var alt    = $('img',html).attr('alt'),
					height = $('img',html).attr('height'),
					src    = $('img',html).attr('src'),
					width  = $('img',html).attr('width');

				$('input#widget-image-'+widget_id+'-alt').attr('value',alt);
				$('input#widget-image-'+widget_id+'-height').attr('value',height);
				$('input#widget-image-'+widget_id+'-src').attr('value',src);
				$('input#widget-image-'+widget_id+'-width').attr('value',width);

				tb_remove();
			}

			return false;
		});
	});
});
/*
[caption id="attachment_619" align="alignnone" width="300" caption="Seed pods on stem, Woodvale"]<a href="http://child.local/child-two/files/2011/01/dsc20050901_105100_212.jpg"><img src="http://child.local/child-two/files/2011/01/dsc20050901_105100_212-300x225.jpg" alt="Seed pods on stem, Woodvale" title="dsc20050901_105100_212" width="300" height="225" class="size-medium wp-image-619" /></a>[/caption]
 */