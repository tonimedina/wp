<?php

class thesis_child_theme_two_shortcode
{
	function __construct()
	{
		add_shortcode('catlist',array($this,'catlist_shortcode'));
	}

	public function catlist_shortcode($atts,$content = null)
	{
		extract(shortcode_atts(array(
			'name'        => '',
			'numberposts' => '2',
		),$atts));

		$the_query = new WP_Query(array(
			'posts_per_page' => $numberposts,
			'category_name'  => $name,
			'post_status'    => 'publish'
		));

		$count = 0;

		if ($the_query->have_posts()) :
			$output = '<div class="teasers_box">';

			while ($the_query->have_posts()) : $the_query->the_post();
				global $post;

				$count++;
				$thumb         = get_post_meta($post->ID,'thesis_thumb',true);
				$alt           = get_post_meta($post->ID,'thesis_thumb_alt',true);
				$default_thumb = get_the_post_thumbnail($post->ID,'default-thumbnail');
				$thumbnail     = get_the_post_thumbnail($post->ID,'thumbnail');
				$title         = get_the_title();
				$permalink     = get_permalink();

				if ($count % 2)
				{
					$output .= '<div class="'.implode(get_post_class('teaser'),' ').'" id="post-'.get_the_id().'">';
				}
				else
				{
					$output .= '<div class="'.implode(get_post_class('teaser teaser_right'),' ').'" id="post-'.get_the_id().'">';
				}
					$output .= '<a class="post_image_link" href="'.$permalink.'" title="'.esc_attr(sprintf(__('Permalink to %s','thesis_child_theme_two'),the_title_attribute('echo=0'))).'">';

					if (!empty($thumb))
					{
						$output .= '<img alt="'.$alt.'" class="thumb aligncenter wp-post-image" height="226" src="'.$thumb.'" title="'.$alt.'" width="300">';
					}
					else
					{
						if (has_post_thumbnail() && !empty($default_thumb))
						{
							$output .= $default_thumb;
						}
						else
						{
							$output .= $thumbnail;
						}
					}

					$output .= '</a>';
					$output .= '<h2 class="entry-title"><a href="'.$permalink.'" rel="bookmark" title="'.esc_attr(sprintf(__('Permalink to %s','thesis_child_theme_two'),the_title_attribute('echo=0'))).'">'.$title.'</a></h2>';
					$output .= '<span class="teaser_avatar">'.get_avatar($post->ID,26).'</span>';
					$output .= '<span class="teaser_author">'._x('by','thesis_child_theme_two').' <span class="author vcard"><a href="'.esc_url(get_author_posts_url(get_the_author_meta('ID'))).'" class="url fn" rel="nofollow">'.get_the_author().'</a></span></span> ';
					$output .= '<abbr class="teaser_date published" title="'.get_the_time('Y-m-d').'">'.get_the_date().'</abbr>';
					$output .= '<div class="format_teaser entry-content">';
						$output .= '<p>'.get_the_excerpt().'</p>';
						$output .= '<p><a class="teaser_link" href="'.$permalink.'" rel="nofollow">'._x('Leer Más &rarr;','thesis_child_theme_two').'</a></p>';
						$output .= '<ul class="st_comments"><li><span class="st_fblike_hcount" displayText="Facebook Like" st_url="'.$permalink.'" st_title="'.$title.'"></span></li><li><span class="st_twitter_hcount" displayText="Tweet" st_url="'.$permalink.'" st_title="'.$title.'"></span></li><li><span class="st_plusone_hcount" displayText="Google +1" st_url="'.$permalink.'" st_title="'.$title.'"></span></li><li>'.get_comments_number($post->ID).'</li></ul>';
					$output .= '</div>';
				$output .= '</div>';
			endwhile;

			$output .= '</div>';

		endif;

		wp_reset_postdata();

		return $output;
	}
}

new thesis_child_theme_two_shortcode();

?>